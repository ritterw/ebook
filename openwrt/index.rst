Open Wrt
==============

OpenWrt_ 是 Linux Torvalds 为了帮助管理 Linux 内核开发而开发的一个开放源码的分布式版本控制软件，
它不同于Subversion、CVS这样的集中式版本控制系统。

.. image:: image/openwrt-logo.png
   :align: left
   :width: 386px
   :height: 96px
   :scale: 55%


在集中式版本控制系统中只有一个仓库（repository），许多个工作目录（working copy），
而像Git这样的分布式版本控制系统中（其他主要的分布式版本控制系统还有BitKeeper、Mercurial、
GNU Arch、Bazaar、Darcs、SVK、Monotone等），每一个工作目录都包含一个完整仓库，
它们可以支持离线工作，本地提交可以稍后提交到服务器上。

分布式系统理论上也比集中式的单服务器系统更健壮，单服务器系统一旦服务器出现问题整个系统就不能运行了，
分布式系统通常不会因为一两个节点而受到影响。



---------------

**参考文档**

.. _OpenWrt: http://www.openwrt.org
