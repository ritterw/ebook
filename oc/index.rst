Objective-C
==============
.. image:: image/openwrt-logo.png
   :align: left
   :width: 386px
   :height: 96px
   :scale: 55%

Objective-C_ 是 Objective-C 是一种通用、高级、面向对象的编程语言。它扩展了标准的ANSI C编程语言，
将Smalltalk式的消息传递机制加入到ANSI C中。它是苹果的OS X和iOS操作系统，及其相关API、Cocoa
和Cocoa Touch的主要编程语言。

Objective-C_ 最初源于 NeXT STEP 操作系统，之后在OS X和iOS继承下来。目前主要支持的编译器有GCC和
LLVM（采用Clang作为前端），苹果公司在Xcode4.0之后均采用LLVM作为默认的编译器。最新的Objective-C特
性也都率先在Clang上实现。

**历史**
1980年代初，Step stone 公司的 Brad Cox 与 Tom Love 发明了 Objective-C。 1981年，当他们两人还在
ITT公司技术中心任职时，接触到了一个名为 SmallTalk-80 的语言，恰巧 Cox 当时对软件设计中的重用问题非
常感兴趣，于是他马上就意识到了 SmallTalk-80 这个语言在软件建构中无法衡量的巨大价值，但同时 Cox 与
Love 也明白，在目前的ITT公司中，引入技术最重要的关键是与 C 语言兼容。

于是 Cox 开始撰写一个 C 语言的预处理器，打算使 C 语言具备些许 Smalltalk 的本领。Cox 很快实现出了一
个可用的 C 语言扩展，此即为 Objective-C语言的前身。到了 1983 年，Cox 与 Love 合伙成立了 Productivity
Products International（PPI）公司，将 Objective-C 及其相关库商品化贩售，并在之后将公司改名为 StepStone。
1986年，Cox 出版了一本关于 Objc 的重要著作《Object-Oriented Programming, An Evolutionary Approach》，
书内详述了 Objective-C 的种种设计理念。

1988年，乔布斯(Steve Jobs)离开苹果公司后成立了 NeXT Computer 公司，NeXT 公司买下 Objective-C 语言的授权，
并扩展了著名的开源编译器GCC 使之支持 Objective-C 的编译。并基于 Objective-C 开发了 AppKit 与 Foundation Kit
等等库，作为 NeXT STEP 的的用户接口与开发环境的基础。虽然 NeXT 工作站后来在市场上失败了，但 NeXT 上的软件工具
却在业界中被广泛赞扬。这促使 NeXT 公司放弃硬件业务，转型为销售NeXTStep（以及OpenStep）平台为主的软件公司。

1992年，自由软件基金会的 GNU 开发环境增加了对 Objective-C 的支持。1994年，NeXT Computer公司和Sun Micro system
联合发布了一个针对 NEXT STEP 系统的标准典范，名为 OPEN STEP。OPEN STEP 在自由软件基金会的实现名称为 GNU step。
1996年12月20日，苹果公司宣布收购 NeXT Software 公司，NEXT STEP/OPEN STEP环境成为苹果操作系统下一个主要发行版本
OS X的基础。这个开发环境的该版本被苹果公司称为Cocoa。


内存管理
================
每个对象内部都保存一个与之相关联的整数,称为引用计数器.

当使用 alloc, new 或者 copy 创建一个对象时, 对象的引用计数器被设置为1.

如果引用计数器为零时,它会自动回收.否则不回去回收.

..
 [[Nsobject alloc] init];

alloc 对应的是调用 release.

一般会重写dealloc方法, 在这里释放相关资源.

可以通过发送retainconton 可以知道内存引数计算器的数量.


@class
=================
@class 是一声明一个新的类, 主要在.h文件中对类进行声明.

在M文件中, 需要继续使用import引用类中的方法.

如果是继承基某个类, 需要导入头文件, 主要引用父类的方法,还有需要知道这个父类的成员变量.

如果只是定义成员变量, 属性, 需要用@class, 只是为了告诉编译译它是一个类.在实现文件中, 才导入

头文件.

block
=================
``block`` 封装一段代码, 可以在任何时候执行.


---------------

**参考文档**

.. _Objective-C: http://https://developer.apple.com/library/ios/referencelibrary/GettingStarted/RoadMapiOSCh/chapters/Languages.html
