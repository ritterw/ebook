.. Richard.Wang 学习笔记 documentation master file, created by
   sphinx-quickstart on Fri Dec  7 13:54:33 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
   http://mynote.readthedocs.org


Richard.Wang 学习笔记
=================================================================

本文档是我个人一些学习工作中的笔记,想把一些东西整理出来.
方便以后使用. 这个图标是我大学时社团的团标.


.. image:: images/logo.png
   :align: left
..   :width: 454px
..   :height: 151px
   :scale: 55%

   
:Authors: 
    Richard.Wang

:Version: 1.1 of 2014/02/20 

:Dedication: To my Grandmother and my wife.

第一部分：Linux System Admin
------------------------------

Linux 运维工具和常用的命令.

.. toctree::
   :maxdepth: 2

   linux/redis
   linux/shell
   linux/nginx
   linux/autotools


第二部分：Python 学习
------------------------------


.. toctree::
   :maxdepth: 2

   python/object



第三部分：Kernel 学习
------------------------------

Linux操作系统是UNIX操作系统的一种克隆系统，它诞生于1991 年的10 月5 日（这是第一次正式向外公布的时间）。

.. image:: images/kernel.png
   :align: left
   :width: 280px
   :height: 210px
   :scale: 55%
   
以后借助于Internet网络，并通过全世界各地计算机爱好者的共同努力，已成为今天世界上使用最多的一种UNIX 类操作系统，
并且使用人数还在迅猛增长。

Linux是一套免费使用和自由传播的类Unix操作系统，
是一个基于POSIX和UNIX的多用户、多任务、支持多线程和多CPU的操作系统。

它能运行主要的UNIX工具软件、应用程序和网络协议。它支持32位和64位硬件。
Linux继承了Unix以网络为核心的设计思想，是一个性能稳定的多用户网络操作系统。

它主要用于基于Intel x86系列CPU的计算机上。这个系统是由全世界各地的成千上万的程序员设计和实现的。
其目的是建立不受任何商品化软件的版权制约的、全世界都能自由使用的Unix兼容产品。


----------------------

.. toctree::
   :maxdepth: 2

   kernel/kgdb


第四部分：English
------------------------------

回想这些年学英语的经历,

第一次学习英语的时候,我记得是在上小学六年级的时候.
那年寒假我的同学一个女生(XLH),上我家说服我妈让我去学英语.

记得学费一个月10元还是20元.
后来,我记得我弟弟还和我妹妹我们都一起学习了.

那时候,教我们的是一个大学生,然后我们回家后就用录音机学习一些简单的词.
现在还记得那些单词.

第二次是上初中了,开了英语课.
第三次是上初二的寒假去参加新概念英语补习班.

用录相机,看情景对话.
然后,会有一个老师给我们讲课.

第四次是上中专, 学的小学英语.对于上过初中的学生来说是一种痛苦.

第五次是上高中,那时候英语对我来说是天书,我也不知道从什么地方下手.

第六次是上大学,那时候花钱请了一个本校外语系的姑娘,她说过了专八.

第七次是华尔街.对我来说英语真的一个魔鬼英语,但它又对我的生活这非常重要.

这部分是学习English的一些笔记.

----------------------

.. toctree::
   :maxdepth: 2

   english/wse





第五部分：SAP学习
------------------------------

SAP 是一个ERP软件.现在将记录FICO模块的学习过程.准备为进入SAP
做好准备.

---------------

.. toctree::
   :maxdepth: 2

   sap/sap


第六部分：PYTHON for weibo             
-----------------------------

.. toctree::
   :maxdepth: 2

   weibo/weibo



第七部分：OpenWrt
-----------------------------
OpenWrt是嵌入式设备上运行的linux系统。

.. image:: images/openwrt-logo.png
   :align: left
   :width: 386px
   :height: 96px
   :scale: 55%

OpenWrt 的文件系统是可写的，开发者无需在每一次修改后重新编译，
令它更像一个小型的 Linux 电脑系统，也加快了开发速度。
你会发现无论是 ARM, PowerPC 或 MIPS 的处理器，都有很好的支持。
并且附带3000左右的软件包，用户可以方便的自定义功能来制作固件。
也可以方便的移植各类功能到openwrt下。

------------------------------------------------------

.. toctree::
   :maxdepth: 2

   openwrt/index


第八部分：Android        
-----------------------------

.. toctree::
   :maxdepth: 2

   android/index
   
第九部分: Objective-c
-----------------------------

.. toctree::
   :maxdepth: 2

   oc/index


第十部分: Meteor
-----------------------------

.. toctree::
   :maxdepth: 2

   meteor/index
   meteor/test
   meteor/task



Thinking 
-----------------------------

.. toctree::
   :maxdepth: 2

   think/think


附录:
-----------------------------

.. toctree::
   :maxdepth: 2
 
   refer/git
   refer/ascii

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   
.. tips warning and note.

.. warning::

   **声明:**

   这是私人的文档,不能与别人共享.  本文档由 Richard.Wang 撰写，保留所有权利.

   `知识共享许可协议 <http://creativecommons.org/licenses/by-nd/3.0/cn/>`_ <<Richard.Wang 学习笔记>> 由 \ `Richard.Wang <http://ritter.readthedocs.org/en/latest>`_  创作.

   采用 \ `知识共享署名-禁止演绎3.0中国大陆许可协议 <http://creativecommons.org/licenses/by-nd/3.0/cn/>`_
   进行许可。

   .. image:: images/license.png
     :align: center