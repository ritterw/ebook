About document
=================

本文档是 Richard.Wang 学习笔记.

记录着学习和生活的点点滴滴.

About License
=====================

这是私人的文档,不能与别人共享.

本文档由 Richard.Wang 撰写，保留所有权利.

.. image:: images/license.png

`知识共享许可协议 <http://creativecommons.org/licenses/by-nd/3.0/cn/>`_
 
Richard.Wang 学习笔记 由 \ `Richard.Wang <http://ritter.readthedocs.org/en/latest>`_  创作.

采用 \ `知识共享署名-禁止演绎3.0中国大陆许可协议 <http://creativecommons.org/licenses/by-nd/3.0/cn/>`_
进行许可。


