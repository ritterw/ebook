SDS (Simple Dynamic String)
================================

是 ``Redis`` 底层所使用的字符串操作函数,
现在就分析一下它的实现等.

sdsnewlen()函数分析
----------------------

先来看一下最基本的数据结构, 
其实 ``sds`` 就是一个 ``char`` 指针， ``sds`` 就是一个别名而已

.. code-block:: c

   typedef char *sds;
   struct sdshdr {
       //保存字符串的长度,但不包括最后结束标记符.
       int len;
       //保存buf还,有多少剩余空间.
       int free;
       //保存字符串的数组.
       char buf[];
    };
    
这个结构在内存里面占8个字节 ``char buf[]`` 这个占0个字节。

这是按32位的 ``CPU`` ,按 ``int`` 类型算的.
其实 ``struct sdshdr`` 就是用来管理 ``sds`` 的。

.. code-block:: c

   sds sdsnewlen(const void *init, size_t initlen) {
       struct sdshdr *sh;
   
       if (init) {
           sh = zmalloc(sizeof(struct sdshdr)+initlen+1);
       } else {
           sh = zcalloc(sizeof(struct sdshdr)+initlen+1);
       }
       if (sh == NULL) return NULL;
       sh->len = initlen;
       sh->free = 0;
       //判断如果字符串为空,那么跳过memcpy,
       //否则将字符串赋值给buf数组.
       if (initlen && init)
           memcpy(sh->buf, init, initlen);
       sh->buf[initlen] = '\0';
       return (char*)sh->buf;
   }

sds s = sdsnew("abc");
^^^^^^^^^^^^^^^^^^^^^^^^^^^

当执行上面这行代码，会在堆内存上面开辟一段连续的空间，具体的空间大小是12个字节的空间。

- 最前面4个字节存的是 ``len`` ，也就是 ``sds`` 的长度
- 其次4个字节存的是 ``free`` ，剩余的空间。
- 最后才是3个才是存的是我们实际的 ``char`` 的信息
- 还在加是最一个'\\0'结束标记符.

根据结构的成员,获得结构体指针
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

也就是每创建一个 ``sds`` ，都会执行上面的操作，
都会申请额外的8个字节才存 ``len`` 和 ``free`` 信息。

既然是一段连续的空间，那么只要已知一个指针，
那么就能拿出 ``struct`` 结构里面的任何数据。

还是用上面那个例子

``sds s = sdsnew("abc");``

这个时候 ``s`` 实际上存的是 ``buf`` 首个 ``char`` 数据的地址。
也就是说向前移8个字节就能到 ``struct sdshdr`` 的 ``len`` 的地址

（准确的说是 ``int len`` 的首地址）。
8个字节刚好就是 ``struct sdshdr`` 占的空间字节的大小。

看下面的图,就说明一切了.

.. image:: image/redis_sdshdr.png

下面看源码具体的实现:

.. code-block:: c
   
   static inline size_t sdslen(const sds s) {
       //把buf的首地址向前移动8个，也是到了结构体的指针,
       //然后,在用结构体的指针去取结构的成员变量len.
       //就获取到字符串的长度.
       struct sdshdr *sh = (void*)(s-(sizeof(struct sdshdr)));
       return sh->len;
   }

一直没有说 ``malloc`` 这块,现在我们回头看一下这块的分析.
这块调用了 ``zmalloc`` 和 ``zcalloc`` 两个函数实现的.
   
.. code-block:: c
  
       if (init) {
           sh = zmalloc(sizeof(struct sdshdr)+initlen+1);
       } else {
           sh = zcalloc(sizeof(struct sdshdr)+initlen+1);
       }

那我们继续追踪代码,看这两个函数是怎么实现的.
这两个函数在 ``zmaillc.c`` 中实现.

.. code-block:: c

   void *zmalloc(size_t size) {
       void *ptr = malloc(size+PREFIX_SIZE);
   
       if (!ptr) zmalloc_oom_handler(size);
   #ifdef HAVE_MALLOC_SIZE
       update_zmalloc_stat_alloc(zmalloc_size(ptr));
       return ptr;
   #else
       *((size_t*)ptr) = size;
       update_zmalloc_stat_alloc(size+PREFIX_SIZE);
       return (char*)ptr+PREFIX_SIZE;
   #endif
   }
   
   void *zcalloc(size_t size) {
       void *ptr = calloc(1, size+PREFIX_SIZE);
   
       if (!ptr) zmalloc_oom_handler(size);
   #ifdef HAVE_MALLOC_SIZE
       update_zmalloc_stat_alloc(zmalloc_size(ptr));
       return ptr;
   #else
       *((size_t*)ptr) = size;
       update_zmalloc_stat_alloc(size+PREFIX_SIZE);
       return (char*)ptr+PREFIX_SIZE;
   #endif
   }

根据字符串分配空间,然后用 ``long`` 类型进行 ``CPU`` 整数倍对齐,
如果是32位的 ``CPU`` ,整数倍数是4,如果是64位的 ``CPU`` 整数倍是8.

因为 ``CPU`` 每次读取数据,是根据 ``CPU`` 位数来的,如果不对齐那么CPU需要
进行多次读取,然后将多次的结果拼接到一起返回给用户.

如果数据正好是按 ``CPU`` 位数存储,那么 ``CPU`` 只需要一次性的读取,可以大大提高程序的效率.
当然,细心读者会发现,最后存储的字符可能不是正好 ``CPU`` 位数的整数倍.

那块这处理就要我们去处理里了.这块的处理的效率远远快于 ``CPU`` 帮你拼接的效率.
如果有感兴趣,可以去读一下 ``GCC lib`` 中的 ``strlen()`` 函数的实现.

分配空间这块直接调用第三方的函数, ``malloc`` 和 ``calloc`` 函数.

.. code-block:: c

   /* Explicitly override malloc/free etc when using tcmalloc. */
   #if defined(USE_TCMALLOC)
   #define malloc(size) tc_malloc(size)
   #define calloc(count,size) tc_calloc(count,size)
   #define realloc(ptr,size) tc_realloc(ptr,size)
   #define free(ptr) tc_free(ptr)
   #elif defined(USE_JEMALLOC)
   #define malloc(size) je_malloc(size)
   #define calloc(count,size) je_calloc(count,size)
   #define realloc(ptr,size) je_realloc(ptr,size)
   #define free(ptr) je_free(ptr)
   #endif


从上面的代码中我们可以看到， ``Redis`` 在编译时，会先判断是否使用 ``tcmalloc`` ，
如果是，会用 ``tcmalloc`` 对应的函数替换掉标准的 ``libc`` 中的函数实现。

其次会判断 ``jemalloc`` 是否使得，最后如果都没有使用才会用标准的 ``libc`` 中的内存管理函数。

而在最新的2.4.4版本中， ``jemalloc`` 已经作为源码包的一部分包含在源码包中，
所以可以直接被使用。而如果你要使用 ``tcmalloc`` 的话，是需要自己安装的。










