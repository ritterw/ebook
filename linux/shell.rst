shell
========================

shell面试题
-------------

查找两天前的文件
::

   按时间,来进行查找:
   find ./ -name *.log -and -mtime +1 -and -mtime -2 -exec ls -al {} \;
   按文件大小来进行查找:
   find ./ -name *.log -and -size +100k -exec ls -al {} \; 
   b=512 bytes c=bytes k=kilobytes m=megabytes g=gigabytes

处理以下文件内容,将域名取出并进行计数排序,如处理:
oldboy.log
::

   http://www.etiantian.org/index.html
   http://www.etiantian.org/1.html
   http://post.etiantian.org/index.html
   http://mp3.etiantian.org/index.html
   http://www.etiantian.org/3.html
   http://post.etiantian.org/2.html

::

   $ awk -F "/" '{print $3}' a.log |sort|uniq -c
   
#查看系统运行多长时间

   $ last | grep "system boot" | head -n 1

文件内容如下：
::

   123abc456
   456def123
   567abc789
   789def567
 
要求输出：
::

   456ABC123
   123DEF456
   789ABC567
   567DEF789
 
一句话思路：以点带面，文字处理，shell不行，awk不行，用sed加正则表达式
::

   sed -r  's/([1-9]{3})([a-f]{3})([1-9]{3})/\3\2\1/;y/abcdef/ABCDEF/'

知识点：-r 支持扩展的正则表达式，跟grep用-P类似。y其实就是tr只是写到sed里面更好看一些，
sed的查找替换，当然还有正则中的分组。sed中的分组可以这么用，awk就不行了只能用&，
而且分组数量不能超过9，即不会有\10出现。

linux下如何添加路由
------------------------

解答：

route 命令方法：

主机路由：
::

   /sbin/route add -host 192.168.2.13 dev eth2
   /sbin/route add -host 202.81.11.91 dev lo

缺省网关路由
::

   /sbin/route add default gw 192.168.1.254
   /sbin/route add default gw 202.11.11.1

网络路由 去往某一网络
::

   /sbin/route add -net 192.168.100.0 netmask 255.255.255.0 dev eth0 //通过eth0设备去连接
   /sbin/route add -net 10.8.0.0  netmask 255.255.255.0 gw 192.168.1.90
   /sbin/route add -net 0.0.0.0  netmask 0.0.0.0 gw 203.84.12.1

查看：

   route -n

删除：

   route del

 

ip route 命令方法：

增加路由，主要是本机可以沟通的网段

   ip route add 192.168.5.0 dev eth0

增加可以通往外部的路由，需通过router

   ip route add 192.168.10.0/24 via 192.168.5.100 dev eth0

增加默认路由

   ip route add default via 192.168.1.2 dev eth0

查看：

   ip route show

删除：

   ip route del 192.168.10.0/24




 - iptable 将80端口转发到8080上.本机IP地址是192.168.11.12
 - DNS的三个状态
   
   A
   change
   
 - 查看进程调用的工具 4 个
::
 strace常用来跟踪进程执行时的系统调用和所接收的信号。 
 在Linux世界，进程不能直接访问硬件设备，当进程需要访
 问硬件设备(比如读取磁盘文件，接收网络数据等等)时，
 必须由用户态模式切换至内核态模式，通 过系统调用访问
 硬件设备。strace可以跟踪到一个进程产生的系统调用,
 包括参数，返回值，执行消耗的时间。
 
 strace使用参数
 -p 跟踪指定的进程
 -f 跟踪由fork子进程系统调用
 -F 尝试跟踪vfork子进程系统调吸入，与-f同时出现时, vfork不被跟踪
 -o filename 默认strace将结果输出到stdout。通过-o可以将输出写入到filename文件中
 -ff 常与-o选项一起使用，不同进程(子进程)产生的系统调用输出到filename.PID文件
 -r 打印每一个系统调用的相对时间
 -t 在输出中的每一行前加上时间信息。 -tt 时间确定到微秒级。还可以使用-ttt打印相对时间
 -v 输出所有系统调用。默认情况下，一些频繁调用的系统调用不会输出
 -s 指定每一行输出字符串的长度,默认是32。文件名一直全部输出
 -c 统计每种系统调用所执行的时间，调用次数，出错次数。
 -e expr 输出过滤器，通过表达式，可以过滤出掉你不想要输出
 http://www.perfgeeks.com/?p=501
 
 
 
 - 进程运行的方式
   1,守护进程,后台进程.
   2,前端运程进程.
   
 - 查看目录下的temp目录,包括子目录
   $ du -k
   
 - 端口号
   $ cat /dev/port
   
 - 加载Lib.so文件
::

   #!/bin/sh 
   export LD_LIBRARY_PATH=${pwd}:${LD_LIBRARY_PATH} 

 - 查看lvs模块是否加载
   $ lsmod | grep ip_vs

 - 加载lvs模块
   $ modprobe ip_vs
   
 - keepalive 的运行机制
 
 - 分析 301 和 302 的区别.访问地址的重定向和重写的分别.
 
 - 什么是反向代理和正向代理.
 
 - 查看LVS连接数量
   $ ipvsadm -L -n 看看现在系统的值是多少
   $ ipvsadm --set 120 30 120 将值调小后，ActiveConn会降下来
   http://salogs.com/2009/09/475/
   http://www.austintek.com/LVS/LVS-HOWTO/HOWTO/LVS-HOWTO.ipvsadm.html#ActiveConn
   
 - lvs脑裂问题的解决方案
::

 为什么drbd+heartbeat会脑裂，是因为由于在故障出现的时候，
 2台主机都认为对方是挂掉，出现抢占资源的情况，因为drbd有磁盘共享的情况，
 这样就导致2台机器都启动本地磁盘，导致出现脑裂。
 
 对付HA系统“裂脑”的对策，目前我所了解的大概有以下几条：
 1)  添加冗余的心跳线，例如双线条线。尽量减少“裂脑”发生机会。
 2)  启用磁盘锁。正在服务一方锁住共享磁盘，“裂脑”发生时，让对方完全
     “抢不走”共享磁盘资源。但使用锁磁盘也会有一个不小的问题，
     如果占用共享盘的一 方不主动“解锁”，另一方就永远得不到共享磁盘。
     现实中假如服务节点突然死机或崩溃，就不可能执行解锁命令。
     后备节点也就接管不了共享资源和应用服务。于是有人在HA中设计了“智能”锁。
     即，正在服务的一方只在发现心跳线全部断开（察觉不到对端）时才启用磁盘锁。
     平时就不上锁了。
 3)  设置仲裁机制。例如设置参考IP（如网关IP），当心跳线完全断开时，
     2个节点都各自ping一下 参考IP，不通则表明断点就出在本端，不仅“心跳”、
     还兼对外“服务”的本端网络链路断了，即使启动（或继续）应用服务也没有用了，
     那就主动放弃竞争，让 能够ping通参考IP的一端去起服务。更保险一些，
     ping不通参考IP的一方干脆就自我重启，以彻底释放有可能还占用着的那些共享资源。
 4） 通过IPMI（如有），设置heartbeat强制关闭故障主机，避免脑裂，保持业务唯一性。

 - 解决图片的缓存
 
 - sqiud port defulte 3128
 
 - 查看php载那些模块
   $ <? phpinfo(); ?>
 
 - 查看apache加载哪些模块
   $ /apachectl -t -D DUMP_MODULES
   


- TCP三次握手的过程
  第一次握手：建立连接时，客户端发送syn包(syn=j)到服务器，
  并进入SYN_SEND状态，等待服务器确认； 
  
  第二次握手：服务器收到syn包，必须确认客户的SYN（ack=j+1），
  同时自己也发送一个SYN包（syn=k），即SYN+ACK包，此时服务器进入SYN_RECV状态； 
  
  第三次握手：客户端收到服务器的SYN＋ACK包，向服务器发送确认包ACK(ack=k+1)，
  此包发送完毕，客户端和服务器进入ESTABLISHED状态，完成三次握手。
  完成三次握手，客户端与服务器开始传送数据.

- Apache两种工作模式
  prefork:Unix平台上的默认（缺省）MPM，
  使用多个子进程，每个子进程只有一个线程。
  每个进程在某个确定的时间只能维持一个连接，
  效率高，但内存占用量比较大。
  
  worker:使用多个子进程，每个子进程有多个线程，
  每个线程在某个确定的时间只能维持一个连接，
  内存占用量比较小，适合高流量的http服务器。
  缺点是假如一个线程崩溃，整个进程就会连同其任何线程一起”死掉”，
  所以要保证一个程式在运行时必须被系统识别为”每 个线程都是安全的”。
    
  $ apachetrl -l [查看当前是那种工作模式]

- 数据库备份
  mysqldump -u root -p test > back-test.sql
  mysqldump -u root -p < back-test.sql
  mysqldump -u root -p lib < back-test.sql  [新建一个lib库]
  
  
- 查看Linux下端口号
  $ netstat -an | grep "80"
  $ lsof -i
  
  
- php 测试连接 mysql
:: code: php

  <?
   $link = mysql_connect("localhost", "root", "passwd");
   if(!$link)
     echo "Faild!!!";
   else 
     echo "OK !";
  ?>

  
cacti and ngnis
-----------------


- 将本址的80端口转发到8080端口上.
  iptables -t nat -A PREROUTING -d 192.168.1.80 -p tcp --dport 80 -j DNAT --to-destination 192.168.1.80:8080