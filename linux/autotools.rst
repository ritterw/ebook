Auto Tools
========================

Kickstart
-------------
自动化安装设置,自己手动去配置安装文件.







Reference:
^^^^^^^^^^^^

* \ `Unbutun server auto install <https://help.ubuntu.com/lts/installation-guide/i386/automatic-install.html>`_
# \ `Kickstart install for redhat <https://access.redhat.com/site/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Installation_Guide/ch-kickstart2.html>`_



Cobbler
----------

1, install cobbler
 $ sudo aptitude install cobbler cobbler-web
 
 $ sudo cobbler check
 
 $ sudo cobbler sync


1, install cobbler
::

 $ unzip cobbler
 $ sudo make install
 $ sudo make devinstall
 $ sudo make webtest
 $ sudo aptitude install python-yaml python-simplejson python-netaddr python-cheetah\
   python-urlgrabber distro-info distro-info-data fence-agents hardlink\
   libapache2-mod-python libapache2-mod-wsgi libcrypt-passwdmd5-perl libgmp10 libnet-telnet-perl\
   libnspr4 libnss3 libperl5.14 libsensors4 libsgutils2-2 libsnmp-base libsnmp15 mtools\
   powerwake python-cobbler python-crypto python-distro-info python-django python-pexpect\
   python-pyasn1 python-twisted python-twisted-conch python-twisted-lore python-twisted-mail\
   python-twisted-names python-twisted-news python-twisted-runner python-twisted-web python-twisted-words\
   sg3-utils snmp syslinux syslinux-common tftpd-hpa
   
 $ cd /etc/apache2/mods-enabled/
 $ sudo ln -s ../mods-available/proxy.load
 $ sudo ln -s ../mods-available/proxy_http.load
 $ sudo ln -s ../mods-available/proxy.conf
 $ sudo ln -s ../mods-available/rewrite.load

 $ sudo htdigest /etc/cobbler/users.digest "Cobbler" cobbler 
 $ sudo vim /etc/cobbler/modules.conf
   [authentication]
    module = authn_configfile
    
 $ sudo service cobblerd restart

http://192.168.11.X/cobbler_web/
   
   
 $ sudo python setup.py



Reference:
^^^^^^^^^^^^
# \ ` Cobbler<https://fedorahosted.org/cobbler/>`_

# \ ` cobbler install for ubuntu server <https://help.ubuntu.com/community/Cobbler/Installation>`_


