KGDB
=======
GDB是GNU开源组织发布的一个强大的UNIX下的程序调试工具。或许，各位比较喜欢那种图形界面方式的,像VCA,BCB等IDE的调试，但如果你是在UNIX平台下做软件，你会发现GDB这个调试工具有比VC、BCB的图形化调试器更强大的功能。所谓“寸有所长，尺有所短”就是这个道理。

Kernel下载与编译
--------------------
**下载kernel**

* http://www.kernel.org
* 下载最新的源码,然后解压.

::

   $ tar xvf linux.tar.gz
   $ cd linux

**kernel 编译**

如果直接在源码目录下进行编译会产生很多临时性的中间文件.这些文件会给我们带来一些不便.为了使源码文件和编译产生的临时文件分开,在源码目录同级目录创产一个新的目录obj.然后进行linux目录进行用大写字母O来指定obj目录的路径.
::

   mkdir obj
   cd linux
   make O=../obj defconfig
   make mrproper
   make O=../obj -j16

文件系统
--------------
从pokylinux.org网站上下载一个他们编译好了的根文件系统的压缩包然后安装上去.
步骤::

   $ dd if=/dev/zero of=./busybox.img bs=1M count=4096 (busybox.img文件是4G大小)
   $ mkfs.ext3 busybox.img
   $ mkdir disk
   $ sudo mount -o loop busybox.img disk
   $ tar xvf core-image-lsb-dev-qemux86-64.tag.bz2 -C disk







qemu X86_64启动参数:
--------------------------
如果你的硬盘是sata接口的，你也许需要将上面的 "root=/dev/hda" 替换为 "root=/dev/sda".

::

   qemu -kernel /usr/src/work/bzImage -append "root=/dev/hda" -boot c -hda /usr/src/work/busybox.img -k en-us
   qemu-system-x86_64 -m 512 -kernel ./bzImage -localtime -append "root=/dev/sda" -boot c -hda ./busybox.img -k en-us -redir tcp:5555::22

将qemu系统的中的linux ssh 22端口映射本地的TCP 5555端口上.可以通过ssh连接到qemu的系统上.这块我试了不行啊.
-redir tcp:5555::22 

**KGDB连接:**

先启动qemu.可以参考下面的命令::

   $ qemu-system-x86_64 -m 512 -kernel ./bzImage -localtime -append "root=/dev/sda kgdboc=ttyS0,115200 kgdbwail"\
    -boot c -hda ./busybox.img -k en-us -serial tcp::4321,server

这时，运行qemu的终端将提示等待远程连接到本地端口4321::

   QEMU waiting for connection on: tcp:0.0.0.0:4321,server

这时需要启动另一个终端,运行GDB使用remote模式连接到刚Kernel.
::

   $ gdb vmlinux
   $ (gdb) target remote localhost:4321 

    Remote debugging using localhost:4321
    kgdb_breakpoint () at kernel/debug/debug_core.c:983
    983		wmb(); /* Sync point after breakpoint */
    (gdb)
     
       or 
   $ ./boot-linux.sh

也可以使用shell脚本::

   #!/bin/bash
   gdb = "target remote localhost:4321"
   gdb vmlinux
   echo $gdb

debugfs 调试
----------------------
在kernel里有一个debugfs文件系统,可能查下当前设置的电源状态.需要在kernel中打开这个CONFIG_DEBUG_FS宏定义.

::

    # mount -t debugfs none /sys/kernel/debugfs
    # cd regulator


LFTP
-------
用lftp命令行进行同步,也可以用crontab来定时同步或备份.

::

    lftp -u user2168641,pwd -e "mirror -R /git/workspack/ritter-code/build/html/ www" www20.subdomain.com

下面看一下关于crontab的相关配置说明::
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
::

   $ crontab -e
   
   * * * * * command 
   | | | | `-------- 表示一个星期中的第几天
   | | | `---------- 表示月份
   | | `------------ 表示一个月份中的第几日
   | `-------------- 表示小时
   `---------------- 是表示分钟


在12月内,每天的早上6点到1 点中,每隔3个小时执行一次 /usr/bin/backup::

   0 6-12/3 * 12 * /usr/bin/backup
      |   `------------------------ 每隔3个小时.
      `---------------------------- 早上6点到12点之间.


MIPI LCD
------------

**查看寄存器**
::

   $ adb root
   $ adb shell
   # mount -t debugfs none /sys/kernel/debug
   # cd /sys/kernel/debug/mdp-dbg/

这个目录下面有三个接口，分别是::

   base
   off
   reg

其中 ``base`` 为只读接口，用于获取需要读取的寄存器组的基地址， ``cat base`` 后获取到下面的信息::

   mdp_base  :    e1900000
   mddi_base :    00000000
   emdh_base :    00000000
   mipi_dsi_base: e5900000


``off`` 接口用于指定需要读取或写入的寄存器地址，比如我们要读取MIPI DSI中的从 ``0x0`` 开始的  ``10`` 个寄存器的值， ``一共有63个寄存器`` 输入以下命令::

   echo "0x0 10 0xe5900000" > off

之后执行 ``cat reg`` 可以读取到这 ``10`` 个寄存器的值，具体结果如下::

   0xe5900000: 00000000 00000000 11111000 11119230 
   0xe5900010: 31211101 3e2e1e0e 00001900 00000000 
   0xe5900020: 02480068 03400020 


``reg`` 接口也可以用来写寄存器，但是一次只能写一个寄存器，具体为上面 ``off`` 中指定的地址,如::

   echo "0x0" > reg

PS：无论是kernel本身还是高通的平台相关代码，都提供了一些很实用的debugfs
接口，比如clk，regulator等，在我们平时的开发和调试中可以使用，我们自己也
可以根据需要写一些接口。


Linux mipi device init
^^^^^^^^^^^^^^^^^^^^^^^^^^

 +---------------+
 | FB0 init      |
 +---------------+
 | MDP init      |
 +---------------+
 | Mipi init     |
 +---------------+
 | LCD driver    |
 +---------------+
 