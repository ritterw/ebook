Meteor
==============
meteor_

Discover meteor
================
下面主要是记录 ``Discover Meteor`` 学习笔记.


Meteor command
================
::

 //创建一个 myApp 应用程序
 $ meteor create myApp

 $ meteor run
 //部署到 meteor.com 服务器上
 $ meteor deploy myApp.meteor.com


增加第三方面包:
================
::

 $ meteor add twbs:bootstrap

 $ meteor add underscore
 //增加支持 coffeescript
 $ meteor add coffeescript
 //去掉autopulish
 $ meteor remove autopublish
 //增加一个路由包
 $ meteor add iron:router
 //增加动画加载图
 $ meteor add sacha:spin
 //增加用记验证支持包
 $ meteor add ian:accounts-ui-bootstrap-3
 $ meteor add accounts-password
 //删除insecure包(恢复数据安全)
 $ meteor remove insecure
 //增加安全检查包
 $ meteor add audit-argument-checks



---------------
**已部署的域名**
http://smartx.meteor.com

http://richard.meteor.com

http://welog-richard.meteor.com   [welog project]

http://rj.meteor.com

http://ipad.meteor.com

http://100.meteor.com   [discover meteor project]

---------------
**参考文档**

.. _meteor: http://www.meteor.com